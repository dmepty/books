﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Xml;
using Books.Models;
using PropertyChanged;

namespace Books.ViewModels
{
    [AddINotifyPropertyChangedInterface]
    class MainViewModel
    {
        public ObservableCollection<Genre> Genres { get; set; } = new ObservableCollection<Genre>();
        public Genre SelectedGenre { get; set; }

        public bool IsListView { get; set; } = true;

        public MainViewModel()
        {
            XmlDocument document = new XmlDocument();
            document.Load("ListBooks.xml");

            XmlElement xmlElement = document.DocumentElement;

            //Находим все разделы
            foreach (XmlNode node in xmlElement)
            {
                Genre genre = new Genre();

                foreach (XmlNode childNode in node.ChildNodes)
                {
                    if (childNode.Name == "genre")
                    {
                        genre.Name = childNode.InnerText;

                        if (!Genres.Any(item => item.Name == genre.Name))
                            Genres.Add(genre);
                    }
                }
            }

            //Добавляем книги в разделы
            foreach (XmlNode node in xmlElement)
            {
                Book book = new Book();
                foreach (XmlNode childNode in node.ChildNodes)
                {
                    switch (childNode.Name)
                    {
                        case "name":
                            book.Name = childNode.InnerText;
                            break;

                        case "authors":
                            foreach (XmlNode author in childNode.ChildNodes)
                                book.Authors.Add(author.InnerText);
                            break;

                        case "description":
                            book.Description = childNode.InnerText;
                            break;

                        case "year":
                            book.Year = Int32.Parse(childNode.InnerText);
                            break;

                        case "image":
                            book.ImagePath = childNode.InnerText;
                            break;

                        case "url":
                            book.URL = new Uri(childNode.InnerText);
                            break;
                        case "genre":
                            book.Genre = childNode.InnerText;
                            break;
                    }
                }

                foreach (var genre in Genres)
                {
                    if (genre.Name == book.Genre)
                        genre.Books.Add(book);
                }
            }

            //Сортировка
            foreach (var genre in Genres)
                genre.Books = new ObservableCollection<Book>(genre.Books.OrderBy(i => i));
        }


        public RelayCommand ChangeViewCommand => new RelayCommand(o =>
        {
            IsListView = !IsListView;
        });
    }
}
