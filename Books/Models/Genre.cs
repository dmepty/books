﻿using System.Collections.ObjectModel;

namespace Books.Models
{
    class Genre
    {
        /// <summary>
        /// Название жанра
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Список книг в данном жанре
        /// </summary>
        public ObservableCollection<Book> Books { get; set; } = new ObservableCollection<Book>();
    }
}
