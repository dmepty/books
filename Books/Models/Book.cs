﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using PropertyChanged;

namespace Books.Models
{
    [AddINotifyPropertyChangedInterface]
    class Book : IComparable<Book>
    {
        /// <summary>
        /// Название книги
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Автор(ы)
        /// </summary>
        public List<string> Authors { get; set; } = new List<string>();

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Год издания
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// Путь к файлу обложки
        /// </summary>
        public string ImagePath { get; set; }

        /// <summary>
        /// Ссылка на электронную версию
        /// </summary>
        public Uri URL { get; set; }

        /// <summary>
        /// Раздел книги
        /// </summary>
        public string Genre { get; set; }

        //Команда открытия ссылки
        public RelayCommand OpenBrowser => new RelayCommand(o => { Process.Start(URL.AbsoluteUri); });

        //Реализация интерфейса IComparable для сравнения
        public int CompareTo(Book book)
        {
            return Name.CompareTo(book.Name);
        }
    }
}
