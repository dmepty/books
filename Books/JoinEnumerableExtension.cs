﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;

namespace Books
{
    public class JoinEnumerableExtension : MarkupExtension, IValueConverter
    {
        public string Separator { get; set; }

        public override object ProvideValue(IServiceProvider serviceProvider) => this;
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => throw new NotImplementedException();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value is IEnumerable<string> enumerable ? string.Join(Separator, enumerable) : null;
        }
    }
}
